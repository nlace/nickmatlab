%Impact Dynamics
%HW 1
%Nick Lace
%Problem 2
clear; clc;

%evaluate single side integral
leimpacts = @(m,v,tf) (pi*m*v)/tf;

% test that we get the sample problem
M = 61 * 10^(-3); %grams to kg
v0 = 44.6; %m/s
tf = 0.4 * 10^(-3); %bring seconds out
a = leimpacts(M,v0,tf);
display(sprintf('Max force = %f N', a))



% Tennis Ball
M = 45 * 10^(-3); %grams to kg
v0 = 42; %m/s
tf = 4 * 10^(-3); %bring seconds out
a = leimpacts(M,v0,tf);
display(sprintf('Tennis Ball Max force = %f N', a))

% Golf Ball
M = 61 * 10^(-3); %grams to kg
v0 = 45; %m/s
tf = 0.6 * 10^(-3); %bring seconds out
a = leimpacts(M,v0,tf);
display(sprintf('Golf Ball Max force = %f N', a))

% Ping Pong Ball
M = 2.5 * 10^(-3); %grams to kg
v0 = 44; %m/s
tf = 0.5 * 10^(-3); %bring seconds out
a = leimpacts(M,v0,tf);
display(sprintf('Ping Pong Ball Max force = %f N', a))

% Hammer
M = 1000 * 10^(-3); %grams to kg
v0 = 1; %m/s
tf = 0.2 * 10^(-3); %bring seconds out
a = leimpacts(M,v0,tf);
display(sprintf('Hammer Max force = %f N', a))
