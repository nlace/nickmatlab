%Impact Dynamics
%HW 1
%Nick Lace
%Problem 5
clear; clc;

bball = .140 ;%kg
bat = .820;%kg


%making final velocity of vall positive (setting V0 to negative)
vball = -45; %m/s
vbat = 30; %m/s

estar = 0.564; %Coefficient of restitution


%(i) Determine effective mass.
m = (bball*bat)/(bball + bat)


%(ii) Determine initial relative velocity.
%v =  vbat - vball
v0 =  vball - vbat
%(iii) Determine compression impulse.
pc = -m*v0
%(iv) Determine terminal impulse.
pf = estar * pc + pc
%(v) Find the speed of bat and ball at the instant of separation.


vbatprimepc = vbat + (m/bat)*v0
vballprimepc = vball - (m/bball)*v0

vbatprime = vbat - (pf/bat)
vballprime = vball + (pf/bball)


%(vi) Determine speed of common contact.
%%(vii) Find initial kinetic energy.
%(viii) Find terminal kinetic energy.
%(ix) Find the kinetic energy responsible to bend the bat after impact (recall the high-speed video footage of baseball game shown in class)