function id = inertiald(rmtrx,mmtrx)



% ra = [1.2000   -2.1333   -4.1333]
% rb =[ -3.8000   -2.1333    4.8667]
% rc =[    4.2000    5.8667   -2.1333]
% 
% 
% %ra = [-2/3 -1/3 0];
% %rb = [4/3 2/3 0];
% %each row is a vector
% rmtrx = [ra; rb; rc]
% 
% %mass matrix
% mmtrx = [0.015 0.018 0.012];
% %mmtrx = [2 1]  % to mimic example 2m and 1m


[r,c] = size(rmtrx);


%needs to be square based on number of columns
placeholder = zeros(c,c);


%solve for the identity portion
% determine r*r
for i = 1:r
    
    
    rowseq=0;
    for j = 1:c
        
        
        %walk from left to right, top to bottom
        entry = rmtrx(i,j);
        mass = mmtrx(i);
        
     % display(sprintf('vect: %f, mass: %f',entry,mass))
        
        rowseq=rowseq + entry^2;
        
    end
    
    %walked through the row now do some numbers
    
    for n = 1:c
    
        %we know that we will be using the first section 3 times, and each
        %resultant will be the summation of each vector's "contribution"
        %so using the zero matrix I defined earlier as a placeholder we can
        %add independent of the normal loop.
        ii = rmtrx(i,n);
       % display(sprintf('i: %f, ii: %f',i,ii))
        
        %now start building ii jj kk ....
        lv = placeholder(n,n);
        thisline = mmtrx(i)*(rowseq - ii^2);
        lv = lv + thisline;
  
        placeholder(n,n)  = lv;
    end
 
    
    
end


%now the diagonal is finished, populate the remaining values!
% it is symetrical - abuse the diagonal!
placeholder;


% I_ij = - [sum| m*r_i*rj  ]


%now we build the rest of the square matrix
%should be of size cxc
for i = 1:c
    %across rows
    rowseq=0;
    for j = 1:c
        %across columns
        if i == j
            %ignore (dont want to overwrite the diagonal
        else
            %loop through 
            
            
            for rr = 1:r
                
                m = mmtrx(rr); %mass for this vector
                
                ri = rmtrx(rr,i);
                rj = rmtrx(rr,j);
                
               % display(sprintf('m:%f,ri:%f, rj:%f',m,ri,rj))
                
                ph = placeholder(i,j);
                newval = ph + m*ri*rj;
                placeholder(i,j) = newval;
                
            end
            %now flip sign
            placeholder(i,j) = -placeholder(i,j);
            
        end
        
    end
    
end

id = placeholder;

end


