%Impact Dynamics
%HW 1
%Nick Lace
%Problem 1
clear; clc;

m1 = 5;% kg mass of block
m2 = 0.008;% kg mass of bullet
vb = 380; %m/s velocity of bullet

mt = m1+m2; %total mass

%initial momentum
M = m2*vb; %m * V

%speed of block
vfb = (M)/ mt; %conservation of momentum shows final speed of block

%kinetic energy prior to collision (treating as point)
T1 = .5 * m2*vb^2;

%kinetic energy post collision
T2 = .5*mt*vfb^2;

%change in kinetic energy (work energy)
U = T2-T1;

p = 100 * abs(U)/T1
