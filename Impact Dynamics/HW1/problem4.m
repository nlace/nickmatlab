%Impact Dynamics
%HW 1
%Nick Lace
%Problem 4
clear; clc;

syms ydot zdot

%first define the vectors

%position
pa = [8 0 3];
pb = [3 0 12];
pc = [11 8 5];

%Velocity
va= [12 13 7];
vb= [6 0 11/3];
vc= [10 31/2 0];




%mass (kg)
ma = .015; 
mb = .018;
mc = .012;

mtot = ma + mb + mc;

%find rho hat
rhohat = (ma*pa + mb*pb + mc*pc)/mtot
%(b) Velocity for center of mass [Hint: Text book Page 14, near equation (1.8)]

%Velocity of center of mass
vhat = (ma*va + mb*vb + mc*vc)/mtot


%position vectors
ra = pa - rhohat;
rb = pb - rhohat;
rc = pc - rhohat;


%(c) Moment of momentum for the system about the center of mass. [Hint: Textbook Page 15, equation (1.9)]
hhat = cross(ra,ma*va) + cross(rb,mb*vb) + cross(rc,mc*vc) 


%(d) Inertia dyadic of the system [Hint: Text book Page 15, Footer note 4]
rmtrx = [ra; rb; rc] ; %make the combined position matrix
mmtrx = [ma; mb; mc] ; %make the mass vector
ihat = inertiald(rmtrx,mmtrx) %build dyadic matrix



%(e) Determine angular velocity [Hint: Text book Page 15, equation (1.9)]
w = inv(ihat) * hhat'

%(f) Determine kinetic energy [Hint: Text book Page 16, equation (1.11)]

T = .5 * (dot(ma*va,va) +dot(mb*vb,vb) +dot(mc*vc,vc) )

%(g) Determine translational kinetic energy [Hint: Text book Page 16, equation(1.11a)]
Tv = .5 * mtot * (dot(vhat,vhat)) 

%(h) Determine rotational kinetic energy [Hint: Text book Page 16, equation(1.11a)]

Tw = .5  * w' * ihat* w

