%Nicholas Lace
%Dyadic question

syms ii ij ik ji jj jk ki kj kk
I = [ii ij ik; ji jj jk; ki kj kk]

[r,n] = size(I)
e = eye(n,n)

%square matrix
Idyad=0
for i= 1:n
    for j = 1:n
        % sum i| sum j|  I_ij * e_i * e_j
            ei =e(:,i);
            ej = e(:,j);
            dyadic = ei * ej'
            Idyad= Idyad + I(i,j) * dyadic
            
    end
    
end

I
Idyad