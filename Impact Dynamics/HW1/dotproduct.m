function dp = dotproduct(v1,v2)
    lv1 = length(v1);
    lv2 = length(v2);
    if lv1 == lv2
        accum=0;
        for i = 1:numel(v1)
            accum = accum + v1(i)*v2(i);
        end
        dp = accum;
    else
        dp = 'dimension mismatch';
    end
end

