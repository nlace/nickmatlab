%Impact Dynamics
%HW 1
%Nick Lace
%Problem 3
clear; clc;

syms ydot zdot

%first define the vectors
pa = [8 0 3];
pb = [3 0 12];
pc = [11 8 5];


va= [12 13 7];
vb= [6 0 zdot];
vc= [10 ydot 0];

%now determine relative vectors
pba = pb - pa;
pca = pc - pa;

vba = vb-va;
vca = vc - va;

%determine zdot
dp = dotproduct(pba, vba);
zdt = solve(dp == 0, zdot);


%determine ydot
dp2 = dotproduct(pca, vca);
ydt = solve(dp2 == 0, ydot);

%substitute solutions into rel vectors
vca = subs(vca, ydot, ydt);
vba = subs(vba, zdot, zdt);

%make sure these are in fact zero
dotproduct(pba, vba)
dotproduct(pca, vca)



zdt
ydt
display(sprintf('Zdot = %f ', zdt))
display(sprintf('Ydot = %f ', ydt))
